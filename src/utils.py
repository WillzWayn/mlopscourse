import requests

def download_url(url, dest):
    r = requests.get(url, allow_redirects=True)
    with open(dest, 'wb') as file:
        file.write(r.content)
    return