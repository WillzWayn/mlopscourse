URL: https://github.com/DataTalksClub/mlops-zoomcamp/tree/main/cohorts/2023


[week1]: ./week1.md
[week2]: ./week2.md

# how to contribute



# Week 1

 
- [Week 1 Exercices][week1]

We'll use the same (https://www.nyc.gov/site/tlc/about/tlc-trip-record-data.page)[(NYC taxi dataset)], but instead of "Green Taxi Trip Records", we'll use "Yellow Taxi Trip Records".
Download the data for January and February 2022.

https://d37ci6vzurychx.cloudfront.net/trip-data/yellow_tripdata_2022-01.parquet
https://d37ci6vzurychx.cloudfront.net/trip-data/yellow_tripdata_2022-02.parquet


# Week 2

- [Week 2 Exercices][week2]

conda env create -f condaEnv.yaml